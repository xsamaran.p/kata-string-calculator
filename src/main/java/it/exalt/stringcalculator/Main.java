package it.exalt.stringcalculator;

import it.exalt.stringcalculator.exception.NegativeNumbersException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        rulesExplanation();
        inputProcessing();
    }

    private static void inputProcessing() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String line;
            StringBuilder input = new StringBuilder();
            do {
                line = br.readLine();
                logger.info(line);
                input.append(System.lineSeparator()).append(line);
            } while (!line.trim().equals(""));
            br.close();
            logger.info("Processing ...");
            StringCalculator stringCalculator = new StringCalculator();
            logger.info("Result : {}", stringCalculator.add(input.toString().trim()));
        } catch (NegativeNumbersException | IOException e) {
            logger.error(e.getMessage());
        }
    }

    private static void rulesExplanation() {
        logger.info("The default separator is : '{}'", StringCalculator.DEFAULT_DELIMITER);
        logger.info("If you want to enter a special delimiter please start your input by : '{}'", StringCalculator.CUSTOM_DELIMITER_IDENTIFIER);
        logger.info("Then please on the 2nd line add your numbers to add");
        logger.info("Be careful, negatives numbers can't be processed");
        logger.info("To finish your input enter a blank line");
        logger.info("Your input : ");
    }


}
