package it.exalt.stringcalculator;

import it.exalt.stringcalculator.exception.NegativeNumbersException;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StringCalculator {

    static final String DEFAULT_DELIMITER = ",";
    static final String LINE_BREAK = System.lineSeparator();
    static final String CUSTOM_DELIMITER_IDENTIFIER = "//";

    public int add(String stringToProcess) {
        if (StringUtils.isBlank(stringToProcess)) {
            return 0;
        }
        List<String> stringsToProcess = new ArrayList<>(Arrays.asList(stringToProcess.split(LINE_BREAK)));
        String delimiter = DEFAULT_DELIMITER;
        if (hasCustomDelimiter(stringsToProcess)) {
            delimiter = getDelimeter(stringsToProcess.get(0));
            stringsToProcess.remove(0);
        }

        Map<Boolean, List<Integer>> numbersList = getPositiveAndNegativeNumbersMap(
                stringsToProcess,
                delimiter
        );

        if (!numbersList.get(false).isEmpty()) {
            throw new NegativeNumbersException(getNegativeNumbers(numbersList.get(false)));
        }

        return numbersList.get(true).stream().reduce(0, Integer::sum);
    }


    private boolean hasCustomDelimiter(List<String> stringsToProcess) {
        return stringsToProcess.get(0).startsWith(CUSTOM_DELIMITER_IDENTIFIER);
    }

    private Map<Boolean, List<Integer>> getPositiveAndNegativeNumbersMap(List<String> stringsToProcess, String delimiter) {
        return Arrays.stream(String.join(delimiter, stringsToProcess).split(delimiter))
                .filter(s -> !StringUtils.isBlank(s))
                .map(Integer::parseInt)
                .collect(Collectors.partitioningBy(integer -> integer >= 0));
    }

    private String getDelimeter(String s) {
        return s.contains(CUSTOM_DELIMITER_IDENTIFIER) ? s.replace(CUSTOM_DELIMITER_IDENTIFIER, "") : DEFAULT_DELIMITER;
    }

    private String getNegativeNumbers(List<Integer> numbersArray) {
        return numbersArray
                .stream()
                .map(Object::toString)
                .collect(Collectors.joining(", "));
    }

    private boolean hasNegatifNumbers(List<List<Integer>> numbersArray) {
        return numbersArray.size() > 1;
    }

}
