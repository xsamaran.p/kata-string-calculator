package it.exalt.stringcalculator;


import it.exalt.stringcalculator.exception.NegativeNumbersException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class StringCalculatorTest {

    private final StringCalculator st = new StringCalculator();


    @ParameterizedTest
    @ValueSource(strings = {""})
    void should_add_to_0_if_payload_is_empty(String payload) {
        assertEquals(0, st.add(payload));
    }

    @ParameterizedTest
    @MethodSource
    void should_add_to_itSelf_if_payload_is_an_accepted_number(String payload, Integer expected) {
        assertEquals(expected, st.add(payload));
    }

    private static Stream<Arguments> should_add_to_itSelf_if_payload_is_an_accepted_number() {
        return Stream.of(
                Arguments.of("1", 1),
                Arguments.of("2", 2),
                Arguments.of("3", 3),
                Arguments.of("4", 4),
                Arguments.of("5", 5)
        );
    }

    @ParameterizedTest
    @MethodSource
    void should_add_to_correct_amount_if_payload_is_an_accepted_group_of_number(String payload, Integer expected) {
        assertEquals(expected, st.add(payload));
    }

    private static Stream<Arguments> should_add_to_correct_amount_if_payload_is_an_accepted_group_of_number() {
        return Stream.of(
                Arguments.of("1,3", 4),
                Arguments.of("1,3,5,7", 16)
        );
    }

    @ParameterizedTest
    @MethodSource
    void should_add_to_correct_amount_if_payload_has_line_breaks(String payload, Integer expected) {
        assertEquals(expected, st.add(payload));
    }

    private static Stream<Arguments> should_add_to_correct_amount_if_payload_has_line_breaks() {
        return Stream.of(
                Arguments.of("1,3,5" + System.lineSeparator() + "7", 16),
                Arguments.of("1,3,5" + System.lineSeparator() + "7" + System.lineSeparator() + "7" + System.lineSeparator() + "7" + System.lineSeparator() + "7" + System.lineSeparator() + "7" + System.lineSeparator() + "7" + System.lineSeparator() + "7", 58)
        );
    }

    @ParameterizedTest
    @MethodSource
    void should_add_to_correct_amount_if_payload_has_custom_separator(String payload, Integer expected) {
        assertEquals(expected, st.add(payload));

    }

    private static Stream<Arguments> should_add_to_correct_amount_if_payload_has_custom_separator() {
        return Stream.of(
                Arguments.of("//:" + System.lineSeparator() + "1:3:5:7", 16),
                Arguments.of("//§" + System.lineSeparator() + "1§3§5§7", 16),
                Arguments.of("//l" + System.lineSeparator() + "1l3l5l7", 16),
                Arguments.of("//l" + System.lineSeparator() + "1l3l5l7l8", 24),
                Arguments.of("//2" + System.lineSeparator() + "1232527", 16)
        );
    }

    @ParameterizedTest
    @MethodSource
    void should_add_to_correct_amount_if_payload_has_custom_separator_and_line_breaks(String payload, Integer expected) {
        assertEquals(expected, st.add(payload));
    }

    private static Stream<Arguments> should_add_to_correct_amount_if_payload_has_custom_separator_and_line_breaks() {
        return Stream.of(
                Arguments.of("//:" + System.lineSeparator() + "1:3:5" + System.lineSeparator() + "7", 16),
                Arguments.of("//§" + System.lineSeparator() + "1§3" + System.lineSeparator() + "5§7", 16),
                Arguments.of("//l" + System.lineSeparator() + "1" + System.lineSeparator() + "3l5l7", 16),
                Arguments.of("//l" + System.lineSeparator() + "1l3l5l7" + System.lineSeparator() + "8", 24),
                Arguments.of("//2" + System.lineSeparator() + "123" + System.lineSeparator() + "527", 16)
        );
    }

    @ParameterizedTest
    @MethodSource
    void should_throw_not_allowed_if_payload_has_negative_numbers(String payload, List<Integer> expected) {
        Throwable exception = assertThrows(NegativeNumbersException.class, () -> st.add(payload));
        assertEquals(
                "negatives not allowed : " + expected.stream().map(Object::toString).collect(Collectors.joining(", ")),
                exception.getMessage()
        );
    }

    private static Stream<Arguments> should_throw_not_allowed_if_payload_has_negative_numbers() {
        return Stream.of(
                Arguments.of("-5", Collections.singletonList(-5)),
                Arguments.of("-5,2,-8", Arrays.asList(-5, -8)),
                Arguments.of("-5,4,5,-7,8,9", Arrays.asList(-5, -7))
        );
    }


    @ParameterizedTest
    @MethodSource
    void should_throw_not_allowed_if_payload_has_negative_numbers_and_custom_separator(String payload, List<Integer> expected) {
        Throwable exception = assertThrows(NegativeNumbersException.class, () -> st.add(payload));
        assertEquals(
                "negatives not allowed : " + expected.stream().map(Object::toString).collect(Collectors.joining(", ")),
                exception.getMessage()
        );
    }

    private static Stream<Arguments> should_throw_not_allowed_if_payload_has_negative_numbers_and_custom_separator() {
        return Stream.of(
                Arguments.of("//:" + System.lineSeparator() + "-5:2:4:465:46:98:79:87:987:987:98:7:45:64:21:31:41", Collections.singletonList(-5)),
                Arguments.of("//l" + System.lineSeparator() + "-5l2l-8", Arrays.asList(-5, -8)),
                Arguments.of("//§" + System.lineSeparator() + "5§-4§5§-7§8§9", Arrays.asList(-4, -7))
        );
    }
}
